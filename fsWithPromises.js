/* For all file operations use promises with fs

Q1. Create 2 files simultaneously (without chaining two function calls one after the other).
Wait for 2 seconds and starts deleting them one after another.
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 

    A. using promises chaining
    B. using callbacks

Q3.  
```js
function login(user, val) {
    if(val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1, 
            name: "Test",
        },
        {
            id: 2, 
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file
}
```

Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    */

/* Q1. Create 2 files simultaneously (without chaining two function calls one after the other).
Wait for 2 seconds and starts deleting them one after another.
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)*/

let fs = require('fs')
let path = require('path')
function writeFile(fileName) {
    return new Promise((resolve, reject) => {

        fs.writeFile(path.join(__dirname, `${fileName}.json`), JSON.stringify({ 1: 'dummyData' }), (err, data) => {
            if (err) {
                err = new Error('Error writing files', err)
                reject(err)
            } else {

                console.log(fileName + ' created')
                resolve()
            }
        })
    })
}
function deleteFile(array) {
    return new Promise((resolve, reject) => {
        for (let index = 0; index < array.length; index++) {
            fs.unlink(path.join(__dirname, `${array[index]}.json`), (err, data) => {
                if (err) {
                    console.log('error deleting file', err);
                } else {
                    console.log('files Deleted');
                }
            })
        }
    })

}
function createTwoFiles(firstFile, secondFile) {
    let fileNameArray = [firstFile, secondFile]
    let fileOne = writeFile(`${firstFile}`)
    let fileTwo = writeFile(`${secondFile}`)

    let del = new Promise((resolve, reject) => {
        setTimeout(() => {
            return deleteFile(fileNameArray)
        }, 2 * 1000)
    })

    Promise.all([fileOne, fileTwo, del]).then(() => {
        console.log('files created');
    }).catch((err) => {
        console.log(err);
    })


}



/*Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 

    A. using promises chaining
    B. using callbacks
*/

// A. using promises chaining

function lipsumData() {
    fetch("https://baconipsum.com/api/?type=all-meat&sentences=100&start-with-lorem=1%22")
        .then((data) => {
            if (data.ok) {
                return data.json();
            }
        })

        .then((data) => {

            return data
        })
        
        .then((data) => {
            return new Promise((resolve, reject) => {
                fs.writeFile(path.join(__dirname, 'lipsumData1.json'), JSON.stringify(data), (err, data) => {
                    if (err) {
                        reject(err)
                    } else {

                        resolve('lipsumData1.json')
                    }
                })
            })
            
            .then((nameData) => {
                return new Promise((resolve, reject) => {
                    fs.readFile(path.join(__dirname, `${nameData}`), (err, data) => {
                        if (err) {
                            reject(err)
                        } else {
                            resolve(nameData)
                        }
                    })
                })
                
                .then((nameData) => {
                    return new Promise((resolve, reject) => {
                        fs.writeFile(path.join(__dirname, `${nameData}`), JSON.stringify(data), (err, data) => {
                            if (err) {
                                reject(err)
                            } else {
                                resolve(nameData)
                            }
                        })
                    })
                })
                
                .then((data) => {
                    fs.unlink(path.join(__dirname, `${data}`), (err, data) => {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log('file-deleted');
                        }
                    })
                })
                
                .catch((err) => {
                    console.log(err);
                })

            })

        })
}

function lipsumWithCallback() {
    fetch("https://baconipsum.com/api/?type=all-meat&sentences=100&start-with-lorem=1%22")
       
    .then((data) => {
            if (data.ok) {
                return data.json();
            }
        })

        .then((data) => {
            fs.writeFile(path.join(__dirname, 'lipsumCallback.json'), JSON.stringify(data), (err, data) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log('file-written');
                    fs.readFile(path.join(__dirname, 'lipsumCallback.json'), (err, data) => {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log('file-read');
                            fs.writeFile(path.join(__dirname, 'lipsumCallback2.json'), JSON.stringify(data), (err, data) => {
                                if (err) {
                                    console.log(err);
                                } else {
                                    fs.unlink(path.join(__dirname, 'lipsumCallback.json'), (err, data) => {
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            console.log('file-deleted');
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        })
}

//Q3.


function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}
function logData(user, activity) {
    return new Promise((resolve, reject) => {
        fs.appendFile(path.join(__dirname, "status.txt"), `${user} - ${activity}  Time - ${new Date()}\n`, (err, data) => {
            if (err) {
                console.log(err);
            } else {
                return resolve()
            }
        })
    })
}

function findLogin(user, id) {
    login(user, id)
        .then((userName) => {
            logData(userName, "Login success")
            getData()
                .then((data) => {
                    let result = data.filter((info) => {
                        return info.id == id
                    });
                    if (result.length !== 0) {
                        logData(user, "GetData success")
                    } else {
                        logData(user, "GetData failure")
                    }
                })

        })
        .catch((err) => {
            console.log(err);
            logData(user, "Login failure")
            getData()
                .then((data) => {
                    let result = data.filter((info) => {
                        return info.id == id
                    });
                    if (result.length !== 0) {
                        logData(user, "GetData success")
                    } else {
                        logData(user, "GetData failure")
                    }
                })
        })
}

